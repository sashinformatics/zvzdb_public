<?php
    class dataRun{

        PUBLIC STATIC $AVAILABLEDATABASETYPES = array("mysql","postgre","sqlserver");

        PRIVATE STATIC $CURRENTDBCONNECTION;
        PRIVATE STATIC $CURRENTDBCONNECTIONTYPE;

        public function __construct($passedDB,$DBType){
            self::$CURRENTDBCONNECTION = $passedDB;
            self::$CURRENTDBCONNECTIONTYPE = $DBType;
        }

        //default way of accessing data for each type of db we know how to interact with
        public static function runQueryForData($query,$assocType=null,$SQLSERVERCURSORTYPE='static',$makeIdKey=true){
            $dataGot = array();
            if(is_resource(self::$CURRENTDBCONNECTION)){
                switch(self::$CURRENTDBCONNECTIONTYPE){

                    //mysql
                    case(self::$AVAILABLEDATABASETYPES[0]):

                        $forcedType = $assocType==null?MYSQL_ASSOC:$assocType;
                        $ret = mysql_query($query,self::$CURRENTDBCONNECTION);
                        if($ret){
                            if(mysql_num_rows($ret)<2){
                                $dataGot = mysql_fetch_array($ret,$forcedType);
                            } else {
                                $returnedArray = array();
                                $index = 0;
                                while($row = mysql_fetch_array($ret,$forcedType)){
                                    $returnedArray[$makeIdKey?(isset($row['id'])?$row['id']:$index):$index] = $row;
                                    $index++;
                                };

                                $dataGot = $returnedArray;
                            };
                        } else {
                            return mysql_error(self::$CURRENTDBCONNECTION);
                        };

                    break;

                    //postgre
                    case(self::$AVAILABLEDATABASETYPES[1]):

                        $forcedType = $assocType==null?PGSQL_ASSOC:$assocType;
                        $ret = pg_query(self::$CURRENTDBCONNECTION,$query);
                        if($ret){
                            if(pg_num_rows($ret) > 0 && pg_num_rows($ret) < 2){
                                $dataGot = pg_fetch_array($ret,0,$forcedType);
                            } else {
                                $returnedArray = array();
                                $index = 0;
                                while($row = pg_fetch_array($ret)){
                                    $returnedArray[$makeIdKey?(isset($row['id'])?$row['id']:$index):$index] = $row;
                                    $index++;
                                };

                                $dataGot = $returnedArray;
                            };
                        } else {
                            return pg_last_error(self::$CURRENTDBCONNECTION);
                        };

                    break;

                    //sql server
                    case(self::$AVAILABLEDATABASETYPES[2]):

                        $forcedType = $assocType==null?SQLSRV_FETCH_ASSOC:$assocType;
                        $ret = sqlsrv_query(self::$CURRENTDBCONNECTION,$query,array(),array("Scrollable"=>$SQLSERVERCURSORTYPE));
                        if($ret){
                            if(sqlsrv_num_rows($ret)<2){
                                $dataGot = sqlsrv_fetch_array($ret,$forcedType);
                            } else {
                                $returnedArray = array();
                                $index = 0;
                                while($row = sqlsrv_fetch_array($ret,$forcedType)){
                                    $returnedArray[$makeIdKey?(isset($row['id'])?$row['id']:$index):$index] = $row;
                                    $index++;
                                };

                                $dataGot = $returnedArray;
                            };
                        } else {
                            return sqlsrv_errors();
                        };
                    break;
                };
            } else {
                // is not resource. return what error?
            };

            return $dataGot;
        }

        //runs a simple query returning either "ok" or the error
        public static function runQuerySimple($query,$returnBoolean=false){

            switch(self::$CURRENTDBCONNECTIONTYPE){
                case(self::$AVAILABLEDATABASETYPES[0]):
                    return (mysql_query($query,self::$CURRENTDBCONNECTION)?($returnBoolean?true:"ok"):($returnBoolean?false:"Please check your query or credentials or retry when the gods of databases have risen from slumber. They are mumbling '<i>".mysql_error()."</i>'. Fascinating. Your query was $query"));
                break;
                case(self::$AVAILABLEDATABASETYPES[1]):
                    return (pg_query(self::$CURRENTDBCONNECTION,$query)?($returnBoolean?true:"ok"):($returnBoolean?false:pg_result_error(self::$CURRENTDBCONNECTION)));
                break;
                case(self::$AVAILABLEDATABASETYPES[2]):
                    return (sqlsrv_query(self::$CURRENTDBCONNECTION,$query)?($returnBoolean?true:"ok"):($returnBoolean?false:sqlsrv_errors()));
                break;
            };
        }

        //returns boolean on a query -- if it exists, returns true else false
        public static function checkIfExists($query){
            switch(self::$CURRENTDBCONNECTIONTYPE){
                case(self::$AVAILABLEDATABASETYPES[0]):
                    $d = mysql_query($query,self::$CURRENTDBCONNECTION);
//                    $row = mysql_fetch_row($d);
//                    return ($row?(($row[0]>0)?true:false):false);
                    return ($d?((mysql_num_rows($d)>=1)?true:false):false);
                break;
                case(self::$AVAILABLEDATABASETYPES[1]):
                    $d = pg_query($query,self::$CURRENTDBCONNECTION);
//                    $row = pg_fetch_row($d);
//                    return ($row?(($row[0]>0)?true:false):false);
                    return ($d?((pg_num_rows($d)>0)?true:false):false);
                break;
                case(self::$AVAILABLEDATABASETYPES[2]):
                    $d = sqlsrv_query($query,self::$CURRENTDBCONNECTION);
//                    $row = sqlsrv_fetch_row($d);
//                    return ($row?(($row[0]>0)?true:false):false);
                    return ($d?((sqlsrv_num_rows($d)>0)?true:false):false);
                break;
            };
        }

        //closes an opened link. useful for moving forward across several similar databases
        public static function closeLink($link){
            $ret = false;
            if(is_resource(self::$CURRENTDBCONNECTION)){
                switch(self::$CURRENTDBCONNECTIONTYPE){
                    case(self::$AVAILABLEDATABASETYPES[0]):
                        $ret = mysql_close($link);
                    break;
                    case(self::$AVAILABLEDATABASETYPES[1]):
                        $ret = pg_close($link);
                    break;
                    case(self::$AVAILABLEDATABASETYPES[2]):
                        $ret = sqlsrv_close($link);
                    break;
                };
            } else {
                // is not resource. return what error?
            };
            return $ret?"link $link closed ok.":"link $link failed closing.";
        }

        //will terminate all connections! including AIS's!
        public static function terminateAllDBConnections(){
            try{mysql_close(self::$CURRENTDBCONNECTION);}catch(Exception $e){};
            try{pg_close(self::$CURRENTDBCONNECTION);}catch(Exception $e){};
            try{sqlsrv_close(self::$CURRENTDBCONNECTION);}catch(Exception $e){};
        }
    }
?>
