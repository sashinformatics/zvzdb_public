<?php //@session_start();
class extraCores{

//sorts multidimentional array using asort
public static function aasort(&$array,$key,$reverse=false){
$sorter = array();
$ret = array();
reset($array);
//            if(isset($array[$key])){
foreach($array as $ii=>$va){
$sorter[$ii]=$va[$key];
};
asort($sorter);
foreach($sorter as $ii=>$va){
$ret[$ii]=$array[$ii];
};
$array=$ret;
//            };

return $reverse?array_reverse($array):$array;
}
//sorts multidimentional array using usort
public static function bbsort(&$array,$key,$reverse=false){
$sorter = array();
$ret = array();
reset($array);
//            if(isset($array[$key])){
foreach($array as $ii=>$va){
$sorter[$ii]=$va[$key];
};
//                asort($sorter);
usort($sorter, function($a, $b){
return strlen($a['anomaliesCount'])<strlen($b['anomaliesCount']);
});
foreach($sorter as $ii=>$va){
$ret[$ii]=$array[$ii];
};
$array=$ret;
//            };

return $reverse?array_reverse($array):$array;
}

/**
* Sort a 2 dimensional array based on 1 or more indexes.
*
* msort() can be used to sort a rowset like array on one or more
* 'headers' (keys in the 2th array).
*
* @param array        $array      The array to sort.
* @param string|array $key        The index(es) to sort the array on.
* @param int          $sort_flags The optional parameter to modify the sorting
*                                 behavior. This parameter does not work when
*                                 supplying an array in the $key parameter.
*
* @return array The sorted array.
*/
function msort($array, $key, $sort_flags = SORT_REGULAR) {
if (is_array($array) && count($array) > 0) {
if (!empty($key)) {
$mapping = array();
foreach ($array as $k => $v) {
$sort_key = '';
if (!is_array($key)) {
$sort_key = $v[$key];
} else {
// @TODO This should be fixed, now it will be sorted as string
foreach ($key as $key_key) {
$sort_key .= $v[$key_key];
}
$sort_flags = SORT_STRING;
}
$mapping[$k] = $sort_key;
}
asort($mapping, $sort_flags);
$sorted = array();
foreach ($mapping as $k => $v) {
$sorted[] = $array[$k];
}
return $sorted;
}
}
return $array;
}

//returns the array key matching a certain value
public static function getArrayKey($multidimensionalArray, $k, $value){
if(count($multidimensionalArray)>0){
foreach ($multidimensionalArray as $key => $array){
if ($array[$k] == $value){
$ret = $key;
};
};
} else {
$ret = "";
};
return $ret;
}

//counts the number of duplicate elements in a simple array
public static function countDuplicatesInArray($array=array(),$duplicatedValue) {
$nb = 0;
foreach($array as $k=>$v) if ($v==$duplicatedValue) $nb++;
return $nb;
}

//gets an employee's image, checks if not exists
public static function getEmployeePictureByUsername($uname,$relativePath="../"){
$uname = strtolower($uname);
$anyPicture = glob("{$relativePath}employee_images/thumbs_generated/postwall/100px/$uname.*");
if(count($anyPicture)>0){
return "{$relativePath}employee_images/thumbs_generated/postwall/100px/".basename($anyPicture[0]);
} else {
return "{$relativePath}employee_images/thumbs_generated/postwall/100px/no-image.jpg";
};
}
public static function getEmployeePictureInFolderByUsername($uname,$relativePath="../",$folder="formal_H200"){
$anyPicture = glob("{$relativePath}employee_images/$folder/$uname.*");
if(count($anyPicture)>0){
return "{$relativePath}employee_images/$folder/".basename($anyPicture[0]);
} else {
return "{$relativePath}employee_images/$folder/100px/no-image.jpg";
};
}

//gets the next element in an array based on the key provided
public static function getNext($array, $key){
//            $currentKey = key($array);
//            while ($currentKey !== null && $currentKey != $key) {
//                $gn = next($array);
//                if($gn===false) reset($array);
//                $currentKey = key($array);
//            };
//            return next($array);
$cs = false;
$index = 0;
foreach($array as $i=>$v){
if($cs){
if($index==0){
return $array[1];
break;
} else {
return $v;
break;
};
} else if($key==$i){
$cs=true;
};
$index++;
};

return reset($array);

/**
*
*
function getNext($array, $keyToSearch){
$conditionToStop = false;

foreach($array as $key => $value){
if($conditionToStop){
return $value;
};

if($key == $keyToSearch){
$conditionToStop = true;
};
};

//return first element of array

}
*
*/
}

public static function enableCaching(){

}

public function mailLdapPasswordUpdate($uname){

}

//useful for when php returns a day number and you need to display the day itself
public static function getDayOfWeekFromDayNumber($dayNumber){
$days = array(1 => "Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday");
return $days[$dayNumber];
}

//returns number of days between dates
public function getNumberDaysBetween($dateStart,$dateEnd=null){
if($dateEnd==null) $dateEnd = date("Y-m-d");

$ds = strtotime($dateStart);
$de = strtotime($dateEnd);

return ($ds-$de)/(24*60*60*60);
}
//returns interval in years, months, days between dates
public function getIntervalBetweenDates($dateStart,$dateEnd=null){
$date1 = new DateTime($dateStart);
$date2 = new DateTime($dateEnd==null?date("Y-m-d H:i:s"):$dateEnd);
$interval = $date1->diff($date2);

return array("y"=>$interval->y,"m"=>$interval->m,"d"=>$interval->d, "H"=>$interval->h,"i"=>$interval->i,"s"=>$interval->s);
}

/**
* gets a previous or following working days
*
* @param mixed $date the starting date
* @param mixed $x the days after, as per strtotime (e.g. 'next weekday','previous weekday','-1 week', etc) -- see http://www.php.net/manual/en/datetime.formats.relative.php
* @param array $excludedDates  array of dates to exclude for calculated date (if in falls in this, it is recalculated)
*/
public static function getXWorkingDay($date=null,$x='previous weekday',$excludedDates=null){
$ret = '';

$ret = date('Y-m-d',strtotime($x,strtotime($date==null?date('Y-m-d'):$date)));

if($excludedDates!=null && in_array($ret,$excludedDates)) $ret = self::getXWorkingDay($ret,$x,$excludedDates);// date('Y-m-d',strtotime('previous weekday',strtotime($ret)));

return $ret;
}

public static function turnDateIntoTimestampLegacy($date="d-m-Y"){
//            13-07-2012 00:00
//echo '<pre>';print_r($date);echo '</pre>';
// if($date=="d-m-Y"){
//                $date = date($date." H:i");
//                $ret = mktime(date("H"),date("i"),0,date("m"),date("d"),date("Y"));
//$ret = strtotime(date($date." H:i"));
//                int mktime ( [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )
//            } else {
//                $date = date_parse_from_format('d-m-Y', $date);
//                $timestamp = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
//                $timetmp = explode(" ",$date);
//                $datetmp = explode("-",$timetmp[0]);
//                if(count($timetmp)>1){
//                    $timetmp = explode(":",$timetmp[1]);
//                } else {
//                    $timetmp = array(0=>0,1=>0);
//                };
//                $ret = mktime((int)$timetmp[0],(int)$timetmp[1],0,(int)$datetmp[1],(int)$datetmp[0],(int)$datetmp[2]);
//            };
//            return $ret;
return strtotime($date);
}

public static function turnDateIntoTimestampLegacyTwo($date="d-m-Y"){
//            13-07-2012 00:00
//echo '<pre>';print_r($date);echo '</pre>';
if($date=="d-m-Y"){
$date = date($date." H:i");
$ret = mktime(date("H"),date("i"),0,date("m"),date("d"),date("Y"));
//                $ret = strtotime($date);
//                int mktime ( [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )
} else {
$date = date_parse_from_format('d-m-Y', $date);
$timestamp = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
$timetmp = explode(" ",$date);
$datetmp = explode("-",$timetmp[0]);
if(count($timetmp)>1){
$timetmp = explode(":",$timetmp[1]);
} else {
$timetmp = array(0=>0,1=>0);
};
$ret = mktime((int)$timetmp[0],(int)$timetmp[1],0,(int)$datetmp[1],(int)$datetmp[0],(int)$datetmp[2]);
};
return $ret;
//            return strtotime($date);
}

//turns a date dd-mm-yyyy hh:mm into a timestamp
public static function turnDateIntoTimestamp($date="d-m-Y",$addTimeDifference=0){
if($date=="d-m-Y"){
$ret = mktime(gmdate("H"),gmdate("i"),gmdate("s"),gmdate("m"),gmdate("d"),gmdate("Y")) + (($addTimeDifference!=0) ? ($addTimeDifference*60*60) : 0);
} else {
$timetmp = explode(" ",$date);
$datetmp = explode("-",$timetmp[0]);
$timetmp = count($timetmp)>1 ? explode(":",$timetmp[1]) : array(0,0);
$ret = @mktime((int)$timetmp[0],(int)$timetmp[1],0,(int)$datetmp[1],(int)$datetmp[0],(int)$datetmp[2]) + (($addTimeDifference!=0) ? ($addTimeDifference*60*60) : 0);
};
return $ret;
}
//turns a date yyyy-mm-dd hh:mm into a timestamp
public static function turnRegularDateIntoTimestamp($date="Y-m-d",$addTimeDifference=0){
if($date=="Y-m-d"){
$ret = mktime(gmdate("H"),gmdate("i"),gmdate("s"),gmdate("m"),gmdate("d"),gmdate("Y")) + (is_numeric($addTimeDifference) ? ($addTimeDifference*60*60) : 0);
} else {
$timetmp = explode(" ",$date);
$datetmp = explode("-",$timetmp[0]);
$timetmp = count($timetmp)>1 ? explode(":",$timetmp[1]) : array(0,0);
$ret = @mktime((int)$timetmp[0],(int)$timetmp[1],0,(int)$datetmp[1],(int)$datetmp[2],(int)$datetmp[0]) + (is_numeric($addTimeDifference) ? ($addTimeDifference*60*60) : 0);
};
return $ret;
}

//gets the currently connected ip (in most cases)
public function getIP(){
$ip="";
if(getenv("HTTP_CLIENT_IP")) $ip = getenv("HTTP_CLIENT_IP");
else if(getenv("HTTP_X_FORWARDED_FOR")) $ip = getenv("HTTP_X_FORWARDED_FOR");
else if(getenv("REMOTE_ADDR")) $ip = getenv("REMOTE_ADDR");
else $ip = "";
return $ip;
}

//uses a logfile to track the number of ips logged into amis for use with a chat system (not implemented or fully tested)
public function howManyIps(){
$filename = "./usarsLAWL.log";
$seconds = 300;
$yourIP = extraCores::getIP();

if (file_exists($filename.".lock")) $readonly = true; else $readonly=false;

$count = 0;
//lock the file
if (!$readonly) $fpLock = fopen($filename.".lock", "w");

//read data ips
$fp = @fopen($filename, "r");
$arIPS=explode ("\n", @fread($fp,filesize($filename)) );
@fclose($fp);

//if file is locked get out
if ($readonly) return count($arIPS);

$s = "";
$already=false;
//update data and search user ip
for ($i=0;$i<count($arIPS);$i++) {

$arData= explode (" ", $arIPS[$i]);

//update your user timer
if ($yourIP==$arData[0]) {
$already=true;
$arData[1]=time();
$arData[2]=isset($_SESSION['username'])?$_SESSION['username']:"";
}

// check if user is old
if (isset($arData[1])&&( time()- (integer)$arData[1] < $seconds )){
$s.=$arData[0]." ".$arData[1]." ".$arData[2]."\n";
$count++;
}

}

if (!$already) {
//your user is new, add it to the list
$s.=$yourIP." ".time()."\n";
$count++;
}

//save the list
$fp = fopen($filename, "w");
fwrite($fp,$s);
fclose($fp);

//remove thr lock
fclose($fpLock);
unlink($filename.".lock");

return $count;
}

//amis's encoding algo
public static function encodeData($data,$salt){
return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $data, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
}

//amis's decoding algo
public static function decodeData($data,$salt){
return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($data), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
}

//tries to rectify accent problems when encountered.
public static function damnTheFrench($txt){
return html_entity_decode(stripslashes($txt));
}

//gets data from the ldap server based on a username
public function getEmployeeLDAPDetailsFromUsername($uname){
$ret = array();
$ldapServer = "astekpc51";
$ldapPort = "389";
$filter = "(uid=$uname)";

$ds=@ldap_connect($ldapServer,$ldapPort);

if($ds){
//                $srch = @ldap_search($ds, "dc=robinson,dc=astekpc51,dc=astekmtius", $filter);
$srch = @ldap_search($ds, "ou=ASTEKMU,ou=People,o=ASTEK,dc=robinson,dc=astekpc51,dc=astekmtius", $filter);
if($srch){
$entries = @ldap_get_entries($ds, $srch);

if(isset($entries['mail'][0])) $ret['email'] = $entries['mail'][0];
if(isset($entries['cn'][0])) $ret['name'] = $entries['cn'][0];
if(isset($entries['uid'][0])) $ret['username'] = $entries['uid'][0];
};
};
return $ret;
}

public static function getEmployeeLDAPAuthentication($uname,$passwd){
$ldapServer = "astekpc51";
$ldapPort = "389";
$dnForMu = "ou=ASTEKMU,ou=People,o=ASTEK,dc=robinson,dc=astekpc51,dc=astekmtius";
$ds=ldap_connect($ldapServer,$ldapPort);

if($ds){
$filter = "(uid=$uname)";
$srch = ldap_search($ds, $dnForMu, $filter);
if($srch){
$entries = ldap_get_entries($ds, $srch);
if($entries['count']>0){
$dn = $entries[0]['dn'];

//process password
$passwd = str_replace(str_split("$%-=+!@[]"),"",$passwd);
$passwd = str_rot13($passwd);
//end password processing

if(@ldap_bind($ds, $dn, $passwd)){

$filter = "(uid=$uname)";
$result_check = @ldap_search($ds,$dn,$filter);
$info_check = @ldap_get_entries($ds, $result_check);
$entry_check = @ldap_first_entry($ds,$result_check);
$attrs = @ldap_get_attributes($ds,$entry_check);

$ret = "okgo";

} else {
//can't bind, wrong password
$ret = "Votre mot de passe est incorrect.";
};
} else {
//logged in but no user details returned
$ret = "Aucune entrée LDAP pour les détails entrées.";
};
} else {
//no user
$ret = "L'identifiant $uname n'est pas valide dans notre repertoire LDAP.\nVeuillez contacter l'administrateur LDAP.";
};
} else {
//ldap fail
$ret = "Pas de connection au serveur LDAP.\nVeuillez re-éssayer dans quelques minutes.";
};

return trim($ret);
}

public static function getEmployeeDetailsByUsername($uname,$existingConnection=false){
if(!is_resource($existingConnection) || $existingConnection==false){
$dbo = new dbOpen(false);
$existingConnection = $dbo->getConnectionTo("base-centralisee");
};
$qryr = new dataRun($existingConnection,dataRun::$AVAILABLEDATABASETYPES[0]);
$ret = $qryr->runQueryForData("SELECT * FROM employe WHERE ldap_username='$uname'");
$qryr->closeLink($existingConnection);
return $ret;
}

public static function getEmployeeDetailsByEmployeeID($empID,$existingConnection=false){
if(!is_resource($existingConnection) || $existingConnection==false){
$dbo = new dbOpen(false);
$existingConnection = $dbo->getConnectionTo("base-centralisee");
};
$qryr = new dataRun($existingConnection,dataRun::$AVAILABLEDATABASETYPES[0]);
$ret = $qryr->runQueryForData("SELECT * FROM employe WHERE id=$empID");
$qryr->closeLink($existingConnection);
return $ret;
}

public static function getDataGetUserDataByLogin($uname){
$dtas = new data();
return $dtas->getUserDataByLogin($uname);
}

public static function getEmployeeList($existingConnection=false,$closeLinkAfter=false,$columns="*",$orderBy="name"){
if(!is_resource($existingConnection) || $existingConnection==false){
$dbo = new dbOpen(false);
$existingConnection = $dbo->getConnectionTo("base-centralisee");
};
$qryr = new dataRun($existingConnection,dataRun::$AVAILABLEDATABASETYPES[0]);
$ret = $qryr->runQueryForData("SELECT $columns FROM employe WHERE actif=1 ORDER BY $orderBy ASC");
if($closeLinkAfter) $qryr->closeLink($existingConnection);
return $ret;
}

public static function getEmployeeProjectAssociationByUsername($uname){
$dbo = new dbOpen(false);
return $dbo->runQueryForData("SELECT p.project_name, em.fullname CP FROM projects p, employee_project emp, employe em WHERE emp.emp_ldap_username='$uname' AND (p.enable=1 AND p.id=emp.proj_id AND p.info=em.id)");
}

//tries to generate thumbnails automatically for certain portions of amis, but unused eventually.
public function generateUserThumbnails($fname){
$genTypes=array(
"profile50px"=>"50,50",
"config200px"=>"200,200"
);

$sizeTypes=array(
"profile30px"=>"30,30",
"config100px"=>"100,100"
);

if(file_exists("../employee_images/formal_H200/$fname")){
foreach($genTypes as $k=>$type){
$size = explode(",",$type);
$this->imageResizeFouFahRah("../employee_images/formal_H200/$fname","-$k",$size[0],$size[1]);
};
} else {
foreach($sizeTypes as $k => $type){
$size = explode(",",$type);
$this->imageResizeFouFahRah("../../employee_images/formal/$fname","-$k",$size[0],$size[1], "{$size[0]}px/");
};
//                echo "user image not found at ../employee_images/formal/$fname";
};

}

//tries to resize images
private static function imageResizeFouFahRah($url,$type,$w,$h, $additionalDir = ""){
include_once("simpleImage.class.php");
//            echo $url;
$ext = substr(strrchr($url,'.'),1);
$image = new SimpleImage();
$image->load($url);
$image->resizeToWidth($w);
$image->resizeToHeight($h);
$image->save("../../employee_images/thumbs_generated/postwall/$additionalDir".basename($url,$ext).$ext);// or die ("generated image could not be saved!");
//            return $image->output();
/*
if(isset($_GET['url'])){
$imgurl = trim($_GET['url']);
if(isset($_GET['w']) && isset($_GET['h'])){
include_once("../core/simpleImage.class.php");
$fileLoc = "../../".$_GET['url'];
$image = new SimpleImage();
$image->load($fileLoc);

$image->resizeToWidth($_GET['w']);
$image->resizeToHeight($_GET['h']);

if(isset($_GET['savetype'])){
$image->save('../employee_images/thumbs_generated/'.basename($fileLoc,".jpg").$_GET['savetype'].'.jpg');
} else {
$image->output();
};

};
};
*/

//            include_once("resize.image.class.php");
//            $image = new Resize_Image;
//            $image->ratio = true;
//            $image->new_width = $w;
//            $image->new_height = $h;
//            $image->image_to_resize = $url;
//            $image->image_end_extra_name = $type;
//            $image->save_folder = '../employee_images/thumbs_generated/';
//            $process = $image->resize();
//            return $process;
}

//returns the handle of an opened file
public static function returnOpenedFile($fileURL,$fMode="w",$returnVerbose=false){
//return file_exists($fileURL)?fopen($fileURL, $fMode):($returnVerbose?"! $fileURL is invalid !":false);
return fopen($fileURL, $fMode);
}

//returns the contents of a file
public static function returnFileContents($fileURL){
$f = extraCores::returnOpenedFile($fileURL,"r");
$ret = @fread($f,filesize($fileURL));
fclose($f);
return $ret;
}

// returns the content of a file in form of array, skipping empty and new lines
public static function returnFileContentsArray($fileURL){
return file($fileURL,FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
}

//saves a file somewhere with a .amis extention
public static function doSaveFile($location,$appname,$content,$returnVerbose=false,$extention='.amis'){
if($location=="") $location = "../../"; // should be app root.
$fileName = $location.$appname.$extention;
$ret = false;
//            if(file_exists($fileName)){
if(!$handle = extraCores::returnOpenedFile($fileName)){
if($returnVerbose) $ret = "Cannot open file ($fileName)";
} else {
if(fwrite($handle, $content) === false) {
if($returnVerbose) $ret =  "Cannot write to file ($fileName)";
} else {
fclose($handle);
$ret = true;
};
};
//            };
return $ret;
}

//        public static function doSaveAdminPageEdit($path,$exactContent,$filename='index.html'){
//            if(is_array($path)) $path = implode("",$path);
//            if(trim($path)!=""){
//$handle = extraCores::returnOpenedFile("../../../".$path.$filename,"w",true);
//				$handle = fopen("../../".$path.$filename,"w");
//				fwrite($handle, $exactContent);
//				fclose($handle);
//
//

//if($handle = extraCores::returnOpenedFile("E:\\webdocs\\vhosts\\intranet.astek.mu\\assets\\pages\\reglementsInternes\\".$filename)){
//
//
//
//                    if(fwrite($handle, $exactContent) === false) {
//cannot write to file!
//                        $ret = false;
//                    } else {
//                        fclose($handle);
//$ret = true;
//						$ret = true;
//                    };

//            };
//        }

//saves an edit
public static function doSaveAdminPageEdit($path,$exactContent){
if(is_array($path)) $path = implode("",$path);
if(trim($path)!=""){
$handle = fopen("../".$path,"w");
$ret = fwrite($handle, $exactContent) ? true : false;
fclose($handle);
return $ret;
};
}



//gets the current enigme for the friday (datecheck = 5 where 5 = friday)
public static function getCurrentOrLastEgnime($datecheck=5){
$egnimePool = extracores::getAllEgnimesInfo();
if(count($egnimePool)>0){
$tsNow = strtotime(date("Y-m-d H:i:s"));;//strtotime("now");//extraCores::turnDateIntoTimestampLegacy();
$ret = array();
foreach($egnimePool as $egnime){
$tsFrom = $egnime['fromTS'];
$tsTo = $egnime['toTS'];
//                    if($tsNow<=$tsTo && $tsNow>=$tsFrom && date("N")==$datecheck){
if($tsNow>=$tsTo && $tsNow<=$tsFrom){
$ret = $egnime;
break;
} else {
if(date("N")==$datecheck && $tsNow>=$tsTo && date("d-m-Y")==date("d-m-Y",$tsTo)){
$ret = $egnime;
} else {
$tsLastFriday = strtotime("last ".extraCores::getDayOfWeekFromDayNumber($datecheck));
if($tsLastFriday<=$tsTo && $tsLastFriday>=$tsFrom){
$ret = $egnime;
} else {
$ret['folderID'] = "noEgnime";
$ret['foldername'] = "noEgnime";
$ret['configID'] = "noEgnime";
$ret['from'] = date("d-m-Y 00:00");
//                                $ret['fromTS'] = connector::$CORESET->turnDateIntoTimestamp(date("d-m-Y 00:00"),0);
$ret['fromTS'] = extraCores::turnDateIntoTimestampLegacy(date("d-m-Y 00:00"));
$ret['to'] = date("d-m-Y 15:30");
//                                $ret['toTS'] = connector::$CORESET->turnDateIntoTimestamp(date("d-m-Y 15:30"),0);
$ret['toTS'] = extraCores::turnDateIntoTimestampLegacy(date("d-m-Y 15:30"));
$ret['title'] = "No egnime data";
$ret['availableTo'] = "*";
};
};
};
};
} else {
$ret['folderID'] = "noEgnime";
$ret['configID'] = "noEgnime";
$ret['from'] = date("d-m-Y 00:00");
$ret['fromTS'] = extraCores::turnDateIntoTimestamp(date("d-m-Y 00:00"));
$ret['to'] = date("d-m-Y 15:30");
$ret['toTS'] = extraCores::turnDateIntoTimestamp(date("d-m-Y 15:30"));
$ret['title'] = "No egnime data";
$ret['availableTo'] = "*";
};
return $ret;
}

//gets an enigme's result based on its folder
public static function getEgnimeResultByFolder($folderID){
$ret = file($folderID."/folge.dll");
foreach($ret as $k=>$r) $ret[$k] = trim($r);
return $ret;
}

//gets the list of enigmes available and their info, in a nice array
public static function getAllEgnimesInfo($egnimedir="../../faceboxes/apps/egnime/"){
$ret = array();
$findex = 0;
foreach(glob($egnimedir."*") as $ind=>$dir){
if(is_dir($dir) && $dir!="./noEgnime"){
if(file_exists($dir."/manifest.txt")){
$dirname = $dir;//str_replace("./","",$dir);
$manifestFile = file($dir."/manifest.txt");
$manifest = explode(",",trim($manifestFile[0]));

$configFile = file($dir."/config.ini");
$config = array();
foreach($configFile as $conf){
$confTmp = explode("=",$conf);
$config[$confTmp[0]] = $confTmp[1];
};

$ID = trim(isset($config['id'])?$config['id']:$dirname);
$title = trim(isset($config['title'])?$config['title']:"untitled");
$label = isset($config['label'])?trim($config['label']):null;
$image = isset($config['image'])?trim($config['image']):null;
$expiredShow = isset($config['expiredShow'])?trim($config['expiredShow']):null;
$hideInput = isset($config['hideInput'])?trim($config['hideInput']):null;

if(isset($config['from'])){
//                            $dateFromTS = extraCores::turnDateIntoTimestamp($config['from'],0);
$dateFromTS = extraCores::turnDateIntoTimestampLegacy($config['from']);
$dateFrom = trim($config['from']);
} else {
$dateFromDir = explode("_",$dirname);
//                            $dateFromTS = extraCores::turnDateIntoTimestamp($dateFromDir[0],0);
$dateFromTS = extraCores::turnDateIntoTimestampLegacy($dateFromDir[0]);
$dateFrom = trim($dateFromDir[0]);
};
if(isset($config['to'])){
//                            $dateToTS = extraCores::turnDateIntoTimestamp($config['to'],0);
$dateToTS = extraCores::turnDateIntoTimestampLegacy($config['to']);
$dateTo = trim($config['to']);
} else {
$dateToDir = explode("_",$dirname);
$dateTo = trim(count($dateToDir)>1? $dateToDir[1] : $dateToDir[0]);
//                            $dateToTS = extraCores::turnDateIntoTimestamp($dateTo,0);
$dateToTS = extraCores::turnDateIntoTimestampLegacy($dateTo);
};

$ret[$findex] = array(
"folderID"=>$dirname,
"foldername"=>dirname($dir),
"configID"=>$ID,
"from"=>$dateFrom,
"fromTS"=>$dateFromTS,
"to"=>$dateTo,
"toTS"=>$dateToTS,
"title"=>$title,
"expiredShow"=>$expiredShow,
"availableTo"=>$manifest
);
if($label!=null) $ret[$findex]['label'] = $label;
if($image!=null) $ret[$findex]['image'] = $image;
if($hideInput!=null) $ret[$findex]['hideInput'] = $hideInput;
$findex++;
};
};
};
return $ret;
}


//the new way





//gets the current poll set up
public static function getCurrentVoting($votingdir="../../faceboxes/apps/voting/"){
$votingsPool = extracores::getAllVotingsInfo($votingdir);
$ret = array();
if(count($votingsPool)>0){
$tsNow = extraCores::turnDateIntoTimestamp();

foreach($votingsPool as $voting){
$tsFrom = $voting['fromTS'];
$tsTo = $voting['toTS'];
$dateEnd = date("d-m-Y",$tsTo);

if($tsNow<=$tsTo && $tsNow>=$tsFrom){
$ret = $voting;
break;
} else {
if($dateEnd==date("d-m-Y")){
$ret = $voting;
$ret['expired'] = true;
break;
} else {
$ret = false;
};

};
};
} else {
$ret = false;
};
//            echo '<pre>';print_r($ret);echo '</pre>';
return $ret;
}

//get list of all polls set up
public static function getAllVotingsInfo($votingdir="../../faceboxes/apps/voting/"){
$ret = array();
$findex = 0;

foreach(glob($votingdir."*") as $ind=>$dir){
if(is_dir($dir) && $dir!=$votingdir."/noVotings"){
if(file_exists($dir."/manifest.txt") && file_exists($dir."/config.ini")){

//getting who can see what
$manifestFile = file($dir."/manifest.txt");
$manifest = explode(",",trim($manifestFile[0]));

//getting all the information concerning the vote
$configFile = file($dir."/config.ini");
$config = array();
$opinionsAr = array();

foreach($configFile as $conf){
$confTmp = explode("=",$conf);

//                            echo '<pre>';print_r($confTmp);echo '</pre>';

if(strpos($confTmp[0],"opinion_")!==false){
$num = str_replace("opinion_","",$confTmp[0]);
if(!isset($opinionsAr[$num]) || !is_array($opinionsAr[$num])) $opinionsAr[$num] = array();
$opinionsAr[$num]['value'] = trim($confTmp[1]);
} else if(strpos($confTmp[0],"link_")!==false){
$num = str_replace("link_","",$confTmp[0]);
if(isset($opinionsAr[$num]) || is_array($opinionsAr[$num])){
$opinionsAr[$num]['link'] = "apps/voting/".str_replace(array(".","/"),"",trim($dir))."/".trim($confTmp[1]);
if(isset($confTmp[2])) $opinionsAr[$num]['link'] .= "=".trim($confTmp[2]);
};
//                                $opinionsAr[$num]['link'] = trim($confTmp[1]);
} else {
$config[$confTmp[0]] = trim($confTmp[1]);
};
};

$ID = $config['id'];
$question = isset($config['question'])?$config['question']:"";
$image = isset($config['image'])?trim($config['image']):"../imgs/Poll.png";
$title = trim(isset($config['title'])?$config['title']:"untitled");

if(isset($config['from'])){
$dateFromTS = extraCores::turnDateIntoTimestamp($config['from']);
$dateFrom = trim($config['from']);
} else {
$dateFromDir = explode("_",$dir);
$dateFromTS = extraCores::turnDateIntoTimestamp($dateFromDir[0]);
$dateFrom = trim($dateFromDir[0]);
};
if(isset($config['to'])){
$dateToTS = extraCores::turnDateIntoTimestamp($config['to']);
$dateTo = trim($config['to']);
} else {
$dateToDir = explode("_",$dir);
$dateTo = trim(count($dateToDir)>1? $dateToDir[1] : $dateToDir[0]);
$dateToTS = extraCores::turnDateIntoTimestamp($dateTo);
};

$ret[$findex] = array(
"folderID"      =>  trim($dir),
"foldername"    =>  trim(dirname($dir)),
"configID"      =>  trim($ID),
"title"         =>  $title,
"question"      =>  trim($question),
"options"       =>  $opinionsAr,
"image"         =>  $image==null?"default.png":$image,
"from"          =>  $dateFrom,
"fromTS"        =>  $dateFromTS,
"to"            =>  $dateTo,
"toTS"          =>  $dateToTS,
"availableTo"   =>  $manifest
);

$findex++;
};
};
};

return $ret;
}







// Get substring between two strings.
// Eg: $fullstring = "this is my [tag]dog[/tag]"; $parsed = get_string_between($fullstring, "[tag]", "[/tag]"); (result = dog) :-)
function getStringBetween($string, $start, $end){
$string = " ".$string;
$ini = strpos($string,$start);
if ($ini == 0) return "";
$ini += strlen($start);
$len = strpos($string,$end,$ini) - $ini;
return substr($string,$ini,$len);
}

// Delete all files in a specific directory then delete the directory itself
function deleteDirectoryAndContents($dir){
foreach(glob($dir . '/*') as $file) {
if(is_dir($file))
rrmdir($file);
else
unlink($file);
}
rmdir($dir);
}

//the inverse of nl2br
function br2nl($string){
return preg_replace('#<br\s*/?>#i', "\r", $string);
}

//returns whether a string contains any element in an array
function stringInArray($string, $array) {
$strungKeywords = explode(" ", $string);
foreach ($array as $value){
if(array_search($value,$strungKeywords)){
return true;
break;
};
};
}

function isSJON($string){
json_decode($string);
return (json_last_error() == JSON_ERROR_NONE);
}

//makes a hex colour lighter
//http://stackoverflow.com/questions/3512311/how-to-generate-lighter-darker-color-with-php
function adjustBrightness($hex, $steps){
// Steps should be between -255 and 255. Negative = darker, positive = lighter
$steps = max(-255, min(255, $steps));

// Format the hex color string
$hex = str_replace('#', '', $hex);
if(strlen($hex) == 3) {
$hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
};

// Get decimal values
$r = hexdec(substr($hex,0,2));
$g = hexdec(substr($hex,2,2));
$b = hexdec(substr($hex,4,2));

// Adjust number of steps and keep it inside 0 to 255
$r = max(0,min(255,$r + $steps));
$g = max(0,min(255,$g + $steps));
$b = max(0,min(255,$b + $steps));

$r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
$g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
$b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);

return '#'.$r_hex.$g_hex.$b_hex;
}

}
?>
