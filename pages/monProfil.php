<?php
    include("../connector.class.php");
    $con = new connector();

    $userdetails = $con->getUserDetails($_SESSION['Login']);
    array_map(function($v){return html_entity_decode($v);}, $userdetails);

    $prs = array();
    $profs = $con->getProfessionList();
    //echo '<pre>';print_r($profs);echo '</pre>';
    if(!is_null($profs) && !empty($profs) && $profs!='' && $profs!=false) foreach ($profs as $p) if(!is_null($p) && !empty($p) && $p['Profession']!='') $prs[] = array('value'=>$p['Profession'],'data'=>$p['Profession']);

    $marital_statuses = array('Marié','Célibataire','Divorcé');
    $etudes_followed = array('College','Universitaire','Doctorat');
?>

<script>
    var professionVars = <?php echo json_encode($prs,true);?>;
    window.setTimeout(function(){
        $('select').chosen({"disable_search": true});
        $('.datepickme').datepick();
        //setUpProfessionAutocomplete();

        $("input[type='password']").hover(
            function(){ $(this).prop('type','text'); },
            function(){ $(this).prop('type','password'); }
        );

    },100);

    function setUpProfessionAutocomplete(){
        $('#professionVar').autocomplete({
            lookup: professionVars,
            onSelect: function (suggestion) {
                alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
        });
    };

    function saveUserProfileChanges(){
        $('#monProfilForm input').removeClass('warning errored');

        $.post('bridge.php?target=save_profile_changes',$('#monProfilForm').serializeArray(),function(d){
        	if(d=="ok"){
                alert("Vos changements ont été appliqués.");
            } else {
                var dta = $.parseJSON(d),
                al = 'Erreures:\n\n';
                $.each(dta,function(i,v){
                    al += '• '+decodeEntities(v.msg)+'\n';
                    $('input#'+v.target).addClass('warning errored');
                    $('input#'+v.target).attr('title',v.msg);
                });
                alert(al);
            };
        },'json');
    };
</script>

<style>
    .required:before{ content:'*'; color:red; }
    table.genericTable tr td.fieldItemHeader{ color:#000; background-color: #ffb745; white-space: nowrap; padding:3px; }
    table.genericTable tr td:not(.fieldItemHeader){ width:70%;text-align:left;padding:3px; }
</style>

<div align="center">
    <form id="monProfilForm">
        <span class="required smallFont"> dénote les champs obligatoires.</span>
        <table class='genericTable' style="width:700px;">
            <?php
            if(!empty($userdetails)){

                echo "<input type='hidden' name='idVar' id='idVar' value='{$userdetails['id']}' />";

                //paroisse
                echo "<tr>
                    <td class='fieldItemHeader'>Paroisse</td>
                    <td>{$userdetails['PAROISSE']}</td>
                </tr>";

                //prenom
                echo "<tr>
                    <td class='fieldItemHeader required'>Prenom </td>
                    <td>
                        <input type='text' id='prenomVar' name='prenomVar' value='{$userdetails['Prenom']}' />
                    </td>
                </tr>";

                //nom
                echo "<tr>
                    <td class='fieldItemHeader required'>Nom</td>
                    <td>
                        <input type='text' id='nomVar' name='nomVar' value='{$userdetails['Nom']}' />
                    </td>
                </tr>";

                //alias
                echo "<tr>
                    <td class='fieldItemHeader'>Alias</td>
                    <td>
                        <input id='aliasVar' name='aliasVar' type='text' value='{$userdetails['alias']}' />
                    </td>
                </tr>";

                //dob
                echo "<tr>
                    <td class='fieldItemHeader required'>Date de naissance</td>
                    <td>
                        <input id='dobVar' name='dobVar' class='datepickme' type='date' value='{$userdetails['DOB']}' />
                    </td>
                </tr>";

                //profession
                echo "<tr>
                    <td class='fieldItemHeader required'>Prof&eacute;ssion</td>
                    <td>
                        <input type='text'name='professionVar' id='professionVar' value='{$userdetails['Profession']}' />
                    </td>
                </tr>";

                //etudes
                echo "<tr>
                    <td class='fieldItemHeader'>Niveau étude</td>
                    <td><select id='etudesVar' name='etudesVar'>";
                    foreach($etudes_followed as $e){
                        echo "<option value='$e'".($userdetails['ETUDE']==$e?' selected="selected"':'').">$e</option>";
                    };
                echo "</select></td>
                </tr>";

                //Statut marital
                echo "<tr>
                    <td class='fieldItemHeader'>Statut marital</td>
                    <td><select id='statutVar' name='statutVar'>";
                    foreach($marital_statuses as $m){
                        echo "<option value='$m'".($userdetails['STATUT']==$m?' selected="selected"':'').">$m</option>";
                    };
                echo "</select></td>
                </tr>";

                //Adresse 1 + 2
                echo "<tr>
                    <td class='fieldItemHeader required'>Adresse</td>
                    <td>
                        <input id='adresse1Var' name='adresse1Var' type='text' value='{$userdetails['ADRESSE1']}' />
                        <input id='adresse2Var' name='adresse2Var' type='text' value='{$userdetails['ADRESSE2']}' />
                    </td>
                </tr>";

                //Telephone mob + home + work
                echo "<tr>
                    <td class='fieldItemHeader'>Telephone</td>
                    <td>
                        <table>
                            <tr>
                                <td class='required' style='width:auto;'>Mobile: </td>
                                <td><input id='telmobVar' name='telmobVar' type='text' value='{$userdetails['TELMOB']}' /></td>
                            </tr>
                            <tr>
                                <td style='width:auto;'>Domicile: </td>
                                <td><input id='telhomeVar' name='telhomeVar' type='text' value='{$userdetails['TELHOME']}' /></td>
                            </tr>
                            <tr>
                                <td style='width:auto;'>Travail: </td>
                                <td><input id='telworkVar' name='telworkVar' type='text' value='{$userdetails['TELWORK']}' /></td>
                            </tr>
                        </table>
                    </td>
                </tr>";

                //email
                echo "<tr>
                    <td class='fieldItemHeader'>Email</td>
                    <td>
                        <input id='emailVar' name='emailVar' type='text' value='{$userdetails['EMAIL']}' />
                    </td>
                </tr>";

                //login
                echo "<tr>
                    <td class='fieldItemHeader required'>Identifiant</td>
                    <td>
                        <input id='loginVar' name='loginVar' type='text' value='{$userdetails['Login']}' />
                    </td>
                </tr>";

                //password reset
                echo "<tr>
                    <td class='fieldItemHeader required'>Mot de passe</td>
                    <td>
                        <input id='mdpVar' name='mdpVar' type='password' value='{$userdetails['MDP']}' />
                    </td>
                </tr>";

                //if is admin, show, else not.
                if($userdetails['ADMIN']==1){
                    echo "<tr>
                        <td class='fieldItemHeader'>Est admin</td>
                        <td style='background-color:#c2fec2;'>Oui</td>
                    </tr>";
                };

            };
            ?>

            <tr>
                <td colspan="2">
                    <input type="button" class="button" onclick="saveUserProfileChanges();" value="Sauvegarder changements" />
                </td>
            </tr>
        </table>
    </form>
</div>

<br /><br /><br /><br />