<?php @session_start();

// Copyright Presley Lacharmante (c) 2014 - 2018. Under GNU Licence 2018. Freely distributable.


//    include_once('core/data.class.php');
//    include_once('connector.class.php');
//    include_once('core/painter.class.php');
    include("connector.class.php");
    $con = new connector();

    $ret = 'You have either not specified a target or the target you tried is not valid.';

    switch($_GET['target']){

        //get user info using ldap username
        case('check_login'):
//            $ret = data::checkLogin($_POST['uname'],$_POST['passwd']);
            $ret = $con::checkLogin($_POST['uname'],$_POST['passwd']);
        break;

        case('do_logout'):
            session_destroy();
            $ret = 'ok';
        break;

        case('GET_USER_INFO'):
            $ret = json_encode($con::getUserDetails($_POST['u']));
        break;

        case('get-session'):
            $ret = json_encode($_SESSION);
        break;

        case('do_search'):
            $ret = json_encode($con::doSearch($_POST));
        break;

        case('save_profile_changes'):
            $ret = json_encode($con::save_profile($_POST));
        break;

        case('get_users_by_paroisse'):
            $ret = json_encode($con::getUserDetailsByParoisse($_POST['selParoisse']));
        break;

        //session interactions
//        case('GET_SESSION_VALUE'):
//            $ret = data::getSessionValue($_POST['key']);
//        break;
//        case('SET_SESSION_VALUE'):
//            $ret = data::setSessionValue($_POST['key'],$_POST['value'])==true ? 'ok':'notok';
//        break;
//        case('GET_SESSION_SUB_VALUE'):
//            $ret = data::getSessionMultiValue($_POST['key'],$_POST['subkey']);
//        break;
//        case('SET_SESSION_SUB_VALUE'):
//            $ret = data::setSessionMultiValue($_POST['key'],$_POST['subkey'],$_POST['value'])==true ? 'ok':'notok';
//        break;

    };

    echo $ret;
?>
