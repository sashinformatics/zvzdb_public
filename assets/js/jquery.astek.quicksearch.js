// $.extend(
//     $.expr[':'], { Contains : "$(a).text().toUpperCase().indexOf(m[3].toUpperCase())>=0"
// });

var AUI = {};

AUI.quicksearch = function(opts){

    if(opts.defaultText==null) opts.defaultText = "Search for website within this page";
    if(opts.minLength==null) opts.minLength = 3;
    if(opts.searchable==null) opts.searchable = ".searchable";
    if(opts.searchInputId==null) opts.searchInputId = "#searchSimple";
    if(opts.highlightCssClass==null) opts.highlightCssClass = "highlight";
    if(opts.highlightCss==null) opts.highlightCss = {"backgroundColor":"orange"};
    if(opts.matchCallback==null) opts.matchCallback = null;
    if(opts.nonMatchCallback==null) opts.nonMatchCallback = null;

    if(opts.addClearButton==null) opts.addClearButton = true;

    clearInputField = function(){
        $(opts.searchInputId).val('').trigger('keyup').val(opts.defaultText);
    };

    Object.size = function(obj) {
        var size = 0, key;
        for (key in obj) if (obj.hasOwnProperty(key)) size++;
        return size;
    };
    
    in_array = function (needle, haystack, argStrict) {
        var key = '',
            strict = !! argStrict;
        if (strict) {
            for (key in haystack) {
                if (haystack[key] === needle) {
                    return true;
                }
            }
        } else {
            for (key in haystack) {
                if (haystack[key] == needle) {
                    return true;
                }
            }
        }
        return false;
    };

    this.init = function(){
        var instance = this;
        
        var srchBx = $(opts.searchInputId);

        if(opts.addClearButton){
            var cb = $("<span id='quicksearchClear' title='Cliquez pour effacer le filtrage courant' onclick=\"clearInputField();\" style='cursor:pointer;background-color:#f2d0d0;border-radius:3px;padding:2px;'><img style='vertical-align:-3px;' src='assets/imgs/cross.png' /></span>");
            cb.insertBefore(srchBx);
            cb.hide();
        };

        $(srchBx).val(opts.defaultText)
        .focus(function(){
            if($(srchBx).val() == opts.defaultText) $(srchBx).val("");
        })
        .blur(function(){
            if($(srchBx).val() == "") $(srchBx).val(opts.defaultText);
        })
        .keyup(function(){
            if ($(srchBx).val().length >= opts.minLength){
                
                if($('#quicksearchClear').not(":visible")){
                    $('#quicksearchClear').show();
                };
                
                var founds = [];
                var notfounds = [];
                var searchText = $.trim($(srchBx).val().replace(' ',' '));
                var regx = new RegExp(searchText, 'i');
                // console.log('Searching in '+$(opts.searchable).length+' elements in '+opts.searchable+'.');
                // alert(searchText.length);
                $(opts.searchable).each(function(i,v){
                    var par = $(v).parents('tr');
                    
                    // var searchTransclass = par.prop('id').split('_');
                    // var targClassetc = par.prop('id');//searchTransclass[searchTransclass.length-1];
                    
                    if(regx.test($(v).text())==true){
                        if(opts.matchCallback!=null) $(opts.matchCallback);
                        par.addClass('highlight');
                        // console.log('found #'+par.prop('id'));
                        founds.push(par);
                    };
                    
                    if(regx.test($(v).text())==false){
                        if(opts.nonMatchCallback!=null) $(opts.nonMatchCallback);
                        par.removeClass('highlight');
                        if(!in_array(par,founds)) notfounds.push(par);
                    };

                });
                
                $.each(notfounds, function(a,b){ $(b).hide(); });
                $.each(founds, function(a,b){ $(b).show(); });

            } else {
                if($('#quicksearchClear')) $('#quicksearchClear').hide();
                $(opts.searchable).each(function(i,v){
                    $(v).parent('tr').show().removeClass('highlight');
                });
            };
        });
    };
};
