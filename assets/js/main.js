function setUpTabTogglers(){
    $('.tabtoggler').each(function(i,v){
        $(v).on('click',function(){
            var tagr = $(v).attr('data-placeholder').toString();
            if($(v).not('.tabtogglerSelected')){
                $('.tabtoggler').removeClass('tabtogglerSelected');
                $(v).addClass('tabtogglerSelected');
                loadPage(tagr);
            };
        });
    });
};

function loadPage(page,params,callback){
    var para = params || {};
    $('#myMainContentHolder').slideUp().html('Veuillez patienter...<img src="assets/imgs/small_loading2.gif" />').slideDown(function(){
        $('#myMainContentHolder').load('pages/'+page+'.php', para, function(){
            if(params!=null && params.mutablelogo!=null && params.mutablelogo!=undefined) $('#mutableLogo > img').attr('src','assets/imgs/'+params.mutablelogo);
            $('#myMainContentHolder').slideDown(function(){
                if(callback!=null && callback!=undefined) $(callback);
            });
        });
    });
};

function doLogout(){
    $.post('bridge.php?target=do_logout',function(data){
        window.location.href='./';
    });
};

function setUpTipsy() {
    if (!$.browser.msie) {
        $(".tooltipW").tipsy({
            gravity: "w",
            live: true,
            delayIn: 500
        });
        $(".tooltipE").tipsy({
            gravity: "e",
            live: true
        });
        $(".tooltipS").tipsy({
            gravity: "s",
            live: true
        });
        $(".tooltipN").tipsy({
            gravity: "n",
            live: true
        });
        $(".tooltipManual").tipsy({
            gravity: "sw",
            trigger: "manual"
        });
        $(".tooltipManualS").tipsy({
            gravity: "s",
            trigger: "manual"
        });
        $(".tooltipManualN").tipsy({
            gravity: "n",
            trigger: "manual"
        });
        $(".tooltipAUTO").tipsy({
            gravity: $.fn.tipsy.autoWE
        });
    };
};

function doSearch(formElem){
    $('#searchAmiResultsLoading').fadeIn();
    $('#searchAmiResults').hide();

    var dta = $(formElem).serializeArray();
    $.post("bridge.php?target=do_search",dta,function(data){
//        if(!$.browser.msie)console.log(data);
        $('#searchAmiResultsLoading').fadeOut(function(){
            reloadElement('pages/rechercherAmi.php','#searchAmiResults',{resultsData:data},function(){ $('#searchAmiResults').slideDown(); });
        });
    });
};

/*reloads an element*/
function reloadElement(url,elem,params,callback){
    if(url!="" || url!=null || url!=undefined){
        if(params==null || params==undefined) params = "";
        $(elem).animate({opacity:0.5},400);
        var tempDiv = $("<div />").load(url+"?rn="+returnPseudoRandom()+' '+elem,params, function(){
            $(elem).replaceWith(tempDiv.contents());
            $(elem).animate({opacity:1},400);
            tempDiv.remove();
            if(callback!=null || callback!=undefined || callback!="") $(callback);
        });
    };
};

/*gets a random number*/
function returnPseudoRandom(){
    return Math.round(Math.random() * 9999999999999999 + 1);
};